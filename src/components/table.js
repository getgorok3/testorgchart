import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import NodeBox from './nodeBox'

var _ = require('lodash');
function getTR(item, allitems, addNew, deleteNode, editNode, addSpace, animation, editingNodeId,
    changeEditingNodeId, editable, rootEditable, nodeStyle, nodeClassName, btnsStyle,
    btnsClassName, lineColor) {
    const children = allitems[item.id];
    const colspan = (children ? children.length : 1) * 2;
    const lines = _.map(_.range(0, colspan), (l, idx) => {
        if (idx === 0) {
            return <td key={idx} className="right-line" style={{ borderColor: lineColor }} />

        } else if (idx + 1 === colspan) {
            return <td key={idx} className="left-line" style={{ borderColor: lineColor }} />

        } else if (idx % 2 === 0) {
            return <td key={idx} className="right-line top-line" style={{ borderColor: lineColor }} />

        } else {
            return <td key={idx} className="left-line top-line" style={{ borderColor: lineColor }} />
        }
    })
    return (
        <table className={addSpace ? 'add-space' : ''}>
            <tbody>

                <tr className="node">
                    <td colSpan={colspan}>
                        <NodeBox
                            node={item}
                            addNew={addNew}
                            deleteNode={deleteNode}
                            editNode={editNode}
                            animation={animation}
                            editingNodeId={editingNodeId}
                            changeEditingNodeId={changeEditingNodeId}
                            editable={editable}
                            rootEditable={rootEditable}
                            nodeStyle={nodeStyle}
                            nodeClassName={nodeClassName}
                            btnsStyle={btnsStyle}
                            btnsClassName={btnsClassName}
                        />
                    </td>
                </tr>

                {children &&
                    <tr className="lines">
                        <td colSpan={colspan}>
                            <div className="down-line" style={{ borderColor: lineColor }} />
                        </td>
                    </tr>
                }

                {children && children.length > 1 &&
                    <tr className="lines">
                        {lines}
                    </tr>
                }

                {children && children.length === 1 &&
                    <tr className="lines">
                        <td colSpan={colspan}>
                            <div className="down-line" style={{ borderColor: lineColor }} />
                        </td>
                    </tr>
                }

                <tr className="nodes">
                    {
                        _.map(children, i => {
                            return (
                                <td
                                    key={i.id}
                                    colSpan={2}
                                >
                                    {getTR(
                                        i,
                                        allitems,
                                        addNew,
                                        deleteNode,
                                        editNode,
                                        children.length > 1,
                                        false,
                                        editingNodeId,
                                        changeEditingNodeId,
                                        editable,
                                        rootEditable,
                                        nodeStyle,
                                        nodeClassName,
                                        btnsStyle,
                                        btnsClassName,
                                        lineColor
                                    )}
                                </td>
                            );
                        })
                    }
                </tr>

            </tbody>
        </table>
    )
}
export default getTR
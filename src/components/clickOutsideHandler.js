import React, { Component } from 'react';
import PropTypes from 'prop-types';
import  { FontAwesomeIcon }  from '@fortawesome/react-fontawesome';

var _ = require('lodash');
class ClickOutsideHandler extends Component {
    constructor(props) {
        super(props);
        this.setWrapperRef = this.setWrapperRef.bind(this);           
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
    }
    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target) && this.props.action) {
            this.props.action(-1);
        }
    }
    render() {
        return (
            <div ref={this.setWrapperRef} className="inline-block">
                {this.props.children}
            </div>
        );
    }
}
ClickOutsideHandler.propTypes = {
    children: PropTypes.element.isRequired,
    action: PropTypes.func
}
export default ClickOutsideHandler
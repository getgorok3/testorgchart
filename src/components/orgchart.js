import React, { Component } from 'react';
import PropTypes from 'prop-types';
import getTR from './table'

var _ = require('lodash');
export default class OrgChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editingNodeId: -1
        }
    }
    changeEditingNodeId = (editingNodeId) => {
        if (editingNodeId !== this.state.editingNodeId) {
            this.setState({ editingNodeId: editingNodeId });
        }
    }
    render() {
        const groupedData = _.groupBy(this.props.data, 'ParentId');
        const animate = this.props.animation && this.props.data.length <= 1;
        const tables = _.map(groupedData[null], item => {
            return (
                <div
                    className="root-container"
                    key={item.id}
                >
                    {getTR(
                        item,
                        groupedData,
                        this.props.addNewChild,
                        this.props.deleteNode,
                        this.props.editNode,
                        true,
                        animate,
                        this.state.editingNodeId,
                        this.changeEditingNodeId,
                        this.props.editable,
                        this.props.rootEditable,
                        this.props.nodeStyle,
                        this.props.nodeClassName,
                        this.props.btnsStyle,
                        this.props.btnsClassName,
                        this.props.lineColor
                    )}
                </div>
            );
        });
        return (
            <div className="org-chart-container">
                {tables}
            </div>
        );
    }
}
OrgChart.propTypes = {
    data: PropTypes.array.isRequired,
    addNewChild: PropTypes.func.isRequired,
    deleteNode: PropTypes.func.isRequired,
    editNode: PropTypes.func.isRequired,
    editable: PropTypes.bool.isRequired
};

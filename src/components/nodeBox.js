import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ClickOutsideHandler from './clickOutsideHandler'

var _ = require('lodash');
class NodeBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            node: props.node,
            editMode: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.node !== this.state.node) {
            this.setState({ node: nextProps.node });
        }
        if (nextProps.editingNodeId !== this.state.node.id) {
            this.setState({ editMode: false });
        }
    }
    addNewChild = () => {
        this.props.addNew(this.state.node.id);
    }
    deleteNode = () => {
        this.props.deleteNode(this.state.node);
    }
    editNode = () => {
        this.setState({ editMode: true });
        this.props.changeEditingNodeId(this.state.node.id);
    }
    handleChange = (e) => {
        let node = _.cloneDeep(this.state.node);
        node.position = e.target.value;
        this.setState({ node: node });
    }
    handleChange2 = (e) => {
        let node = _.cloneDeep(this.state.node);
        node.name = e.target.value;
        this.setState({ node: node });
    }
    handleEnterKey = (e) => {
        if (e.key === 'Enter') {
            this.done(e);
        }
    }
    done = () => {
        this.props.editNode(this.state.node);
        this.setState({ editMode: false });
    }
    cancel = () => {
        this.setState({ node: this.props.node, editMode: false });
    }
    render() {
        const {
            node,
            editMode
        } = this.state;

        const {
            editingNodeId,
            changeEditingNodeId,
            editable,
            rootEditable,
            nodeStyle,
            nodeClassName,
            btnsStyle,
            btnsClassName
        } = this.props;

        return (
            <ClickOutsideHandler action={editingNodeId !== -1 && editingNodeId === node.id ? changeEditingNodeId : null}>
                {
                    editMode && editable ?
                        <div className={`node-box ${nodeClassName}`} style={nodeStyle} >
                            <div className={`org-node-btns ${btnsClassName}`} style={btnsStyle}>
                                <FontAwesomeIcon icon="check" className="icon" onClick={this.done} />
                                <FontAwesomeIcon icon="times" className="icon" onClick={this.cancel} />
                            </div>
                            <textarea
                                rows="3"
                                cols="10"
                                autoFocus
                                value={node.position}
                                onChange={this.handleChange}
                                onKeyPress={this.handleEnterKey}
                                onBlur={this.onBlur}
                            />
                            <textarea
                                rows="3"
                                cols="10"
                                autoFocus
                                value={node.name}
                                onChange={this.handleChange2}
                                onKeyPress={this.handleEnterKey}
                                onBlur={this.onBlur}
                            />
                        </div>
                        :
                        <div className={`node-box ${this.props.animation ? 'node-animation' : ''} ${nodeClassName}`} style={nodeStyle} >
                            {editable &&
                                <div className={`org-node-btns ${btnsClassName}`} style={btnsStyle}>
                                    <FontAwesomeIcon icon="plus" className="icon" onClick={this.addNewChild} />
                                    {(node.ParentId !== null || (node.ParentId === null && rootEditable)) &&
                                        <FontAwesomeIcon icon="pencil-alt" className="icon" onClick={this.editNode} />
                                    }
                                    {node.ParentId !== null &&
                                        <FontAwesomeIcon icon="trash-alt" className="icon" onClick={this.deleteNode} />
                                    }
                                </div>
                            }
                            <div onClick={(node.ParentId !== null || (node.ParentId === null && rootEditable)) && editable ? this.editNode : undefined} className="node-name-box">
                        
                                    <div >
                                        <img src="https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=1&w=500" alt="Smiley face" height="auto" width="50 px"></img>
                                    </div>
                                    <div>
                                        <div >
                                            <p className="node-name">
                                                {/* {_.truncate(node.position, { length: 10, omission: ' ...' }) } */}
                                                {node.position}
                                                {/* <br /> */}
                                                {/* <p className="node-name"> */}
                                                {/* {_.truncate(node.name, { length: 10, omission: ' ...' })} */}
                                                {/* {node.name} */}
                                                {/* </p> */}
                                            </p>
                                        </div>
                                        <div>
                                            <p className="node-name">
                                                {node.name}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <br />

                            </div>
                      
                }
            </ClickOutsideHandler>
        );
    }
}
NodeBox.propTypes = {
    node: PropTypes.object.isRequired,
    addNew: PropTypes.func.isRequired,
    deleteNode: PropTypes.func.isRequired,
    editNode: PropTypes.func.isRequired,
    editingNodeId: PropTypes.number.isRequired,
    changeEditingNodeId: PropTypes.func.isRequired,
    editable: PropTypes.bool.isRequired
}
export default NodeBox